# Variables and Expressions

A quick heads up before we get started. Even if you are already familiar with variables from other programming languages, AutoHotkey does things a little differently. I'd still catch this one.

This video is about using variables in AutoHotkey. If you've never heard that word before, a variable is simply allows you to name a piece of data, such as a number. Over time as your program runs the data can be referenced or updated as needed, that way you wont' have to type the same data in everywhere in your code. 

You can name a variable whatever you want, as long as it doesn't start with a number. Don't start a variable with a number >.< In a perfect world your variable names should describe what kind of data you want to store in them. 

I haven't actually explained what data you can put in a variable yet though. We're going to go over two basic data types that AutoHotkey uses: Numbers and Strings.

## Numbers

First we're gonna cover numbers. You can write any number you want an AutoHotkey will figure out what you mean.

To set a value to a variable you type the variable name first, colon equals, and finally the variable value. I'll use 100 to show the number type.

> Number := 100

We can also use decimal places to create floating point numbers like PI using a decimal point.

> Number := 3.14159

We can also do math operations like additional and multiplication following order of operations rules

> Number := ((1 + 1) * 2) / 2
> Number is 2

We can also reference other variables in place of a number. In this example I make the height variable twice of the number width variable.

> Width := 100
> Height := Width * 2
> Height is 200


Following so far? Let's see if you can solve these variable problems?

###### QUIZ TIME ######

Number 1:

> Apples := 5
> Pears := Apples * 3
> Oranges := Pears - 1

Whats the value of Oranges? 

Number 2:

> Cars := (2 * 1) + (6 / 2) - 2

Whats the value of Cars? (Answer: 3)

###### QUIZ TIME OUT ######

## Strings

The next type of variable is a string. A string is a collection of letters. If you want to store some text in your variable you'll need to use strings. Strings are wrapped in quotation marks to tell autohotkey that you are reffering to a string and not another variable name. Let's use our colon equal operator again, and then a quotation mark. Now we can type in whatever set of letters we want. How about Hello String!

> Text := "Hello string!"

We can also combine strings together using the dot operator as shown here. 

> HelloWord := "Hello "
> StringWord := "String"
> Combined := HelloWord . StringWord

Note that we added a spacebar at the end of HelloWord. the dot operator takes the two strings and smashes them together, so if you want a space between words, you have to add it. 

We often need to combine numbers and strings together to format our output. The dot operator is perfect for this as well.

> Math := 250 * 16 + 4
> Answer := "The answer is " . Math

The Answer variable would have the string `the answer is 4004`. 

## Using Variables

Variables alone in your script do nothing. The real power comes in when you use them as inputs to other commands like we talked about in the previous video. For example, consider our Answer script from before. When we run this script nothing happens. But if we used a MsgBox command we can take the answer variable and create a message box with our answer! To use a variable in another command you need to put percent signs on both ends like this.

> Math := 250 * 16 + 4
> Answer := "The answer is " . Math
> MsgBox, %Answer%

When we run this program now we get a message box to appear with our output. Later as we talk about manipulating the mouse and keyboard in more detail, we'll be able to use variables in other commands to manipulate our computer and mouse at an even greater level. Hope to see you there.


I wish I could cover all the little details about variables because there is a lot to learn, but this is enough to get you started. I do want to warn you about one thing though before we leave this topic. I taught you to use colon equals to set your variables, but there's a different mode called legacy mode where you only use the equal sign. You'll find this all over the internet when looking at scripts online. There's a completely different set of rules for using this way, so just be aware of that as you come across it. Personally I find it easy to screw up so I never use that method.